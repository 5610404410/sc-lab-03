package View;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Control.Controller;

public class GUI{
	private JFrame frame;
	private JPanel panelText;
	private JPanel panelResult;
	private JTextArea text;
	private JTextArea result;
	private JButton submit;
	private JRadioButton ngram;
	private JRadioButton word;
	private Controller control;
	private String str;
	private String message;
	private String numberNgram;
	
	class ListenerMgr implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (ngram.isSelected()) {
				setResult(control.getValueNgram(text.getText(),Integer.parseInt(numberNgram)));
				extendResult(String.valueOf(control.getValueCountNgram()));
			} else if (word.isSelected()) {
				result.setText(control.getValueWord(text.getText()));
			}
		}
	}
	
	public GUI(){
		control = new Controller();
		createFrame();
	}
	
	public void createFrame(){
		frame = new JFrame();
		frame.setSize(450,235);
		frame.setLayout(null);
		text = new JTextArea();
		result = new JTextArea();
		
		//text box		
		panelText = new JPanel();
		panelText.setLayout(null);
		panelText.setBounds(10, 10, 202, 120);
		frame.add(panelText);
		text.setBorder(BorderFactory.createLineBorder(Color.black));
		text.setBounds(10, 10, 183, 101);
		panelText.add(text);
		
		//result box
		panelResult = new JPanel();
		panelResult.setLayout(null);
		panelResult.setBounds(222, 10, 202, 120);
		frame.add(panelResult);
		result.setBorder(BorderFactory.createLineBorder(Color.black));
		result.setBounds(10, 10, 183, 101);
		panelResult.add(result);
		
		//summit button
		submit = new JButton("submit");
		submit.setBounds(275, 150, 100, 25);
		frame.add(submit);
		
		submit.addActionListener(new ListenerMgr());
		
		//ngram radioButton
		ngram = new JRadioButton("n-gram");
		ngram.setSelected(true);
		ngram.setBounds(25, 150, 75, 25);
		frame.add(ngram);
		
		//word radioButton
		word = new JRadioButton("การแยกคำ");
		word.setBounds(100, 150, 100, 25);
		frame.add(word);
				
		frame.setVisible(true);
		groupbutton();	
		message = JOptionPane.showInputDialog("Enter your message : ");
		text.setText(message);
		numberNgram = JOptionPane.showInputDialog("Enter n :");
	}
	
	public void setResult(String str){
		this.str = str;
		result.setText(str);
	}
	
	public void extendResult(String str) {
		this.str = this.str+"\n"+str;
		result.setText(this.str);
	}
	
	public String getMessage(){
		return message;
	}
	
	public String getNumberNgram(){
		return numberNgram;
	}
	
	public void groupbutton(){
		ButtonGroup btn = new ButtonGroup();
		
		btn.add(ngram);
		btn.add(word);	
	}
}
