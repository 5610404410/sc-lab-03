package Model;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class Ngram {
	private StringTokenizer text;
	private int count;
	
//	public Ngram(){
//		result = new ArrayList<String>();
//	}
	
	public String wordTokenizer(String str){
		String newText = "";
		text = new StringTokenizer(str);
		int amount = text.countTokens();
		while(text.hasMoreTokens()){
			newText += text.nextToken();
		}
		return String.valueOf(amount);
	}
	
	public ArrayList<String> generateNgram(String str, int number){
		ArrayList<String> result = new ArrayList<String>();
		String m = str;
		String totalString = "";
		StringTokenizer token = new StringTokenizer(m);
		
		while(token.hasMoreTokens()){
			totalString += (String) token.nextToken();
		}
		
		for (int i = 0; i <= totalString.length(); i++){
			if (number + i <= totalString.length()){
				result.add(totalString.substring(i,i+number));
			}
			else{
				return result;
			}
			count = result.size();
		}
		return result;
	}
	
	public int countNgram(){
		return count;
	}
}
