package Control;

import java.awt.event.ActionListener;

import Model.Ngram;
import View.GUI;

public class Controller {
		private String message;
		private String numberNgram;
		private GUI gui;
		private Ngram ngram;
		
	public Controller(){	
		ngram = new Ngram();
	}
	
	public String getValueWord(String str){
		String b = ngram.wordTokenizer(str);
		return b;
	}
	
	public String getValueNgram(String str, int number){
		String a = ngram.generateNgram(str,number).toString();
		return a;
	}
	public int getValueCountNgram(){
		return ngram.countNgram();
	}
}
